## Azure Kubernetes 

## Topics

####  Azure Service
- [ ] Basics of Kubernetes 
- [ ] Create Azure AKS Cluster using Azure Portal
- [ ] 	Docker Fundamentals
- [ ] 	Imperative Method: Kubernetes Fundamentals using kubectl
- [ ] 	Declarative Method: Kubernetes Fundamentals using YAML
- [ ] 	Azure Disks for AKS Storage
- [ ] 	Custom Storage Class, PVC and PV
- [ ] 	AKS default Storage class, PVC and PV
- [ ] 	User Management Web Application Deployment with MySQL as storage using Storage Class, PVC and PV
- [ ] 	Azure MySQL for AKS Storage
- [ ] 	Kubernetes Secrets
- [ ] 	Azure Files for AKS Storage
- [ ] 	Ingress Basics
- [ ]    Ingress Context path based Routing
- [ ] 	Azure DNS Zones - Delegate domain from AWS to Azure
- [ ] 	Ingress and External DNS with Azure DNS Zones
- [ ] 	Ingress Domain Name based Routing with External DNS
- [ ] Ingress SSL with LetsEncrypt
- [ ] Kubernetes Requests & Limits
- [ ] 	Kubernetes Namespaces
- [ ] 	Kubernetes Namespaces - Imperative
- [ ] 	Kubernetes Namespaces - Limit Range
- [ ] 	Kubernetes Namespaces - Resource Quota
- [ ] 	Azure Virtual Nodes for AKS
- [ ] 	Azure Virtual Nodes Basics
- [ ] 	Azure AKS Virtual Nodes Mixed Mode Deployments
- [ ] 	Azure Container Registry for AKS
- [ ] 	Integrate Azure Container Registry ACR with AKS
- [ ] 	Azure AKS Pull Docker Images from ACR using Service Principal
- [ ] 	Pull Docker Images from ACR using Service Principal and Run on Azure Virtual Nodes

- [ ] 	Azure AKS - Enable HTTP Application Routing AddOn
- [ ] 	Azure AKS Authentication with Azure AD and Kubernetes RBAC
- [ ] 	Azure AKS Cluster Access with Multiple Clusters
- [ ] 	Azure AD Integration with Azure AKS for Authentication
- [ ] 	Kubernetes RBAC Role & Role Binding with Azure AD on AKS
- [ ] 	Kubernetes RBAC Cluster Role & Role Binding with AD on AKS
- [ ] 	Azure AKS Cluster Autoscaling
- [ ] 	Azure AKS - Cluster Autoscaler
- [ ] 	Azure AKS - Horizontal Pod Autoscaler HPA
- [ ] 	Azure AKS Production Grade Cluster Design using AZ AKS CLI
- [ ] 	Create Azure AKS Cluster using AZ AKS CLI
- [ ] 	Create Azure AKS Linux, Windows and Virtual Node Pools
- [ ] 	Deploy Apps to Azure AKS Linux, Windows and Virtual Node Pools
- [ ] 	Provision Azure AKS Clusters using Terraform

- [ ] 	Create AKS Cluster Linux and Windows Node Pools
- [ ] 	Create Azure AKS Cluster using Custom Virtual Network



